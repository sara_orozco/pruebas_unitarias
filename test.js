const instPrueba = require('.');


test('instPrueba falla si no hay número entre el 0 y el 5', () => {
    expect(instPrueba('0') || instPrueba('1') || instPrueba('2') || instPrueba('3') || instPrueba('4') || instPrueba('5')).toBe(true);
 
});

test('instPrueba falla si no está la secuencia DEF', () => {
    expect(instPrueba('DEF')).toBe(true);
});

test('instPrueba falla si no aparece la letra "X', () => {
    expect(instPrueba('X')).toBe(true);

});

//#1
test('instPrueba FALLA no HAY "a"', () => {
    expect(instPrueba('a')).toBe(true);
});
//#2
test('instPrueba FALLA si HAY "c" o "C"', () => {
    expect(instPrueba('c')).toBe(false);
    expect(instPrueba('C')).toBe(false);
});
//#3
test('instPrueba FALLA si HAY SECUENCIA "ABC"', () => {
    expect(instPrueba('ABC')).toBe(false);    
});